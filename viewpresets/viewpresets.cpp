#include <QUuid>
#include <QDebug>
#include <QCryptographicHash>

#include "viewpresets.h"
#include "settings.h"


ViewPresets::ViewPresets(CamsList &cams, QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);

    selectedView = -1;

    // Load view presets from Configs Manager
    cfg = Settings::instance();

    QStringList ids = cfg->getParam("presets.ids").toStringList();
    for (int i = 0; i < ids.size(); ++i)
    {
        ViewItem preset;

        preset.id        = ids.at(i);
        preset.isUpdated = false;
        preset.isNew     = false;
        preset.name      = cfg->getParam("presets." + preset.id + ".name").toString();
        preset.columns   = cfg->getParam("presets." + preset.id + ".columns").toInt();

        if (preset.columns <= 0)
        {
            preset.columns = 1;
        }

        preset.camUrls = cfg->getParam("presets." + preset.id + ".camUrls").toStringList();

        presets.append(preset);
    }


    // Fill presets list
    for (int i = 0; i < presets.size(); ++i)
    {
        viewList->addItem(presets.at(i).name);
    }

    // Fill cams list
    camsInfo = cams.getCams();
    for (int i = 0; i < camsInfo.size(); ++i)
    {
        QListWidgetItem *item = new QListWidgetItem(camsInfo.at(i).description, this->camsList);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        item->setCheckState(Qt::Unchecked); // hack??? else it does not display as checkable item
    }

}


void ViewPresets::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}



void ViewPresets::on_buttonBox_accepted()
{
    QStringList ids = cfg->getParam("presets.ids").toStringList();

    // Remove existings items
    for (int i = 0; i < removed.size(); ++i)
    {
        const QString &id = removed.at(i).id;
        ids.removeOne(id);

        cfg->removeParam("presets." + id + ".name");
        cfg->removeParam("presets." + id + ".columns");
        cfg->removeParam("presets." + id + ".camUrls");
    }

    // Add new items and update existings
    for (int i = 0; i < presets.size(); ++i)
    {
        const ViewItem &item = presets.at(i);
        if (!item.isUpdated)
        {
            continue;
        }

        ids.append(item.id);
        cfg->setParam("presets." + item.id + ".name",    item.name);
        cfg->setParam("presets." + item.id + ".columns", item.columns);
        cfg->setParam("presets." + item.id + ".camUrls", item.camUrls);
    }

    ids.removeDuplicates();
    cfg->setParam("presets.ids", ids);

    accept();
}


void ViewPresets::on_buttonBox_rejected()
{
    reject();
}


void ViewPresets::fillViewItem(ViewItem &viewItem)
{
    viewItem.isUpdated = true;
    viewItem.isNew     = false;
    viewItem.columns   = columnCount->value();
    viewItem.name      = viewName->text();

    // Fill selected cams
    viewItem.camUrls.clear();
    for (int i = 0; i < camsInfo.size(); ++i)
    {
        QListWidgetItem *listItem = camsList->item(i);
        if (listItem->checkState() == Qt::Checked)
        {
            viewItem.camUrls.append(camsInfo.at(i).url);
        }
    }
}


void ViewPresets::on_addView_clicked()
{
    ViewItem viewItem;

    fillViewItem(viewItem);

    QString hashString = viewItem.name;
    for (int i = 0; i < viewItem.camUrls.size(); ++i)
    {
        hashString += viewItem.camUrls.at(i);
    }

    viewItem.id        = QCryptographicHash::hash(hashString.toUtf8(), QCryptographicHash::Sha1).toHex();
    viewItem.isNew     = true;

    presets.append(viewItem);
    viewList->addItem(viewItem.name);
    viewList->setCurrentRow(presets.size() - 1, QItemSelectionModel::ClearAndSelect);
}


void ViewPresets::on_replaceView_clicked()
{
    if (selectedView < 0 || selectedView >= presets.size())
    {
        return;
    }

    ViewItem &viewItem = presets[selectedView];
    fillViewItem(viewItem);

    viewList->item(selectedView)->setText(viewItem.name);
}


void ViewPresets::on_removeView_clicked()
{
    if (selectedView < 0)
    {
        return;
    }

    int currentRow = selectedView;
    ViewItem viewItem = presets.at(selectedView);
    presets.removeAt(selectedView);

    if (!viewItem.isNew)
    {
        removed.append(viewItem);
    }

    QListWidgetItem *item = viewList->takeItem(currentRow);
    delete item;

    currentRow--;
    viewList->setCurrentRow(currentRow);
    if (currentRow == -1 && presets.size() > 0)
    {
        viewList->setCurrentRow(0, QItemSelectionModel::ClearAndSelect);
    }
    else if (currentRow + 1 < presets.size())
    {
        viewList->setCurrentRow(currentRow + 1, QItemSelectionModel::ClearAndSelect);
    }

}


void ViewPresets::on_viewList_itemSelectionChanged()
{
    QListWidgetItem *item = viewList->currentItem();
    int row = viewList->row(item);
    qDebug() << "Item selection changed to: " << row;

    selectedView = row;

    if (row < 0)
    {
        return;
    }

    if (row >= presets.size())
    {
        return;
    }

    ViewItem &viewItem = presets[selectedView];

    columnCount->setValue(viewItem.columns);
    viewName->setText(viewItem.name);

    // fill cam list
    for (int i = 0; i < camsInfo.size(); ++i)
    {
        CamInfo &info = camsInfo[i];

        if (viewItem.camUrls.contains(info.url))
        {
            camsList->item(i)->setCheckState(Qt::Checked);
        }
        else
        {
            camsList->item(i)->setCheckState(Qt::Unchecked);
        }
    }
}
