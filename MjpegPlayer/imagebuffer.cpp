#include <QDebug>
#include <QTime>

#include "imagebuffer.h"

ImageBuffer::ImageBuffer(int size)
    : ringBuffer(false),
      blockReadOnEmptyQueue(true)
{
    if (size > 0)
    {
       bufferSize = size;
    }
    else
    {
        bufferSize = 1;
    }
}

ImageBuffer::~ImageBuffer()
{
    imageQueue.clear();
    qDebug() << "ImageBuffer" << "dtor";
}

/* Add a frame to the buffer.  
 * The data in the frame is copied to the internal image buffer.  
 * If there are no images in the buffer, the clients of the wait condition are woken to notify them of a new frame.
 * If the buffer is full, the function blocks until it there is space
 * @param image The image to add
 */
void ImageBuffer::addFrame(const QByteArray &image)
{

	if(image.isEmpty() || image.isNull())
	{
		qDebug() << "E: Imagebuffer received a null image";
		return;
	}

    mutex.lock();
    if(imageQueue.size() >= bufferSize)
	{
        if (isRingBuffer())
        {
            // Drop old buffer from Queue
            imageQueue.dequeue();
        }
        else
        {
            bufferNotFull.wait(&mutex);
        }
	}
    mutex.unlock();
	
    mutex.lock();
    // copy the image
    imageQueue.enqueue(image);
    mutex.unlock();

    bufferNotEmpty.wakeAll();
}

/* Return the oldest frame from the buffer.  
 * This is a blocking operation, so if there are no available images, this function will block until one is available or image acquisition is stopped.
  * @returns IplImage pointer to the next available frame.  Ownership of the data is passed to the callee, so the image must be released
 */
QByteArray ImageBuffer::getFrame(unsigned long time)
{
    QByteArray temp;

    if (blockReadOnEmptyQueue)
    {
        mutex.lock();
        if(imageQueue.isEmpty())
        {
            bufferNotEmpty.wait(&mutex, time);
        }
        mutex.unlock();
    }

    mutex.lock();
    if(!imageQueue.isEmpty())
	{
        temp = imageQueue.dequeue();
	}
    mutex.unlock();

    bufferNotFull.wakeAll();

	return temp;
}

/* Clear the buffer and release clients */
void ImageBuffer::clear()
{
    imageQueue.clear();
    bufferNotEmpty.wakeAll();
}

void ImageBuffer::setBlockingReadOnEmptyQueue(bool blockReadOnEmptyQueue)
{
    this->blockReadOnEmptyQueue = blockReadOnEmptyQueue;
}
