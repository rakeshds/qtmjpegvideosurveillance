#ifndef IMAGE_BUFFER_H
#define IMAGE_BUFFER_H

#include <QWaitCondition>
#include <QMutex>
#include <QQueue>

/*
 * The image buffer buffers images between the capture thread and the processing thread and provides synchronization between the two threads.
 */

class ImageBuffer
{
public:
    explicit ImageBuffer(int size);
    virtual ~ImageBuffer();

	void addFrame(const QByteArray &frame);
    QByteArray getFrame(unsigned long time = ULONG_MAX);
	void clear();

    void setBlockingReadOnEmptyQueue(bool blockReadOnEmptyQueue);

    bool isRingBuffer() {return ringBuffer;}
    void setRingBufferEnabled(bool ringBufferEnabled = true) {this->ringBuffer = ringBufferEnabled;}

private:
	QWaitCondition     bufferNotEmpty;
	QWaitCondition     bufferNotFull;
	QMutex			   mutex;
    int				   bufferSize;
    bool               ringBuffer;

    /** Queue of QByteArray frame objects to store the data. **/
	QQueue<QByteArray> imageQueue;

    bool                blockReadOnEmptyQueue;
};

#endif
