#include <QDebug>

#include "mediacontroll.h"
#include "settings.h"


//// Ctors and Dtors ///////////////////////////////////////////////////////////////////////////////
MediaControll::MediaControll(QWidget *parent) :
    QWidget(parent),
    doReload(false),
    autoRepeat(false),
    forceStop(false),
    isPaused(false)
{
    setupUi(this);

    buffer = QSharedPointer<ImageBuffer>(new ImageBuffer(Settings::instance()->getParam("core.frame.buffered_count").toInt()));
    buffer->setRingBufferEnabled(true);
    mediaObject = new MjpegMediaObject(buffer, this);

    processThread = new ProcessThread(buffer, this);

    processThread->setRenderWidget(videoPort);
    processThread->start();

    videoPort->setTitle(tr("No title..."));
    controlButtonsWidget->setVisible(false);

    connect(videoPort, SIGNAL(mouseEnter()),
            this,      SLOT(onVideoPortMouseEnter()));
    connect(videoPort, SIGNAL(mouseLeave()),
            this,      SLOT(onVideoPortMouseLeave()));
    connect(videoPort, SIGNAL(mouseButtonReleased(Qt::MouseButton)),
            this,      SLOT(onVideoPortClicked(Qt::MouseButton)));

    connect(mediaObject, SIGNAL(finished()),
            this,         SLOT(onFinished()));
    connect(mediaObject, SIGNAL(error(int)),
            this,         SLOT(onError(int)));

    stopButton->setEnabled(false);
    playButton->setEnabled(true);
    reloadButton->setEnabled(false);
}


MediaControll::~MediaControll()
{
    const long waitTime = 5000;

    processThread->done();
    processThread->exit();
    if (!processThread->wait(waitTime))
    {
        processThread->terminate();
        processThread->wait(waitTime);
    }

    //mediaObject->done();
    //mediaObject->exit();
    //if (!mediaObject->wait(waitTime))
    //{
    //    mediaObject->terminate();
    //    mediaObject->wait(waitTime);
    //}

    // Store to log before delete
    QString title = videoPort->getTitle();
    QString url   = mediaObject->getUrl();

    //
    //delete buffer;
    // media object has no parent, so delete manually
    //delete mediaObject;
    //processThread->deleteLater();

    qDebug() << "Media control" << title << " & " << url << "stopped";
    qDebug() << this << "~dtor";
}


//// Events Handlers ///////////////////////////////////////////////////////////////////////////////
void MediaControll::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type())
    {
        case QEvent::LanguageChange:
            //retranslateUi(this);
            break;
        default:
            break;
    }
}


//// Private Methods ///////////////////////////////////////////////////////////////////////////////


//// Public Methods ////////////////////////////////////////////////////////////////////////////////
void MediaControll::load(const QString &url)
{
    mediaObject->load(url);
}


void MediaControll::play()
{
    if (isPaused)
    {
        isPaused = false;
        processThread->setPaused(false);

        stopButton->setEnabled(true);
        playButton->setEnabled(false);
        reloadButton->setEnabled(true);
    }
    else
    {
        bool result = mediaObject->play();
        // TODO: check result
        if (result)
        {
            stopButton->setEnabled(true);
            playButton->setEnabled(false);
            reloadButton->setEnabled(true);
        }
    }
}


void MediaControll::stop()
{
    mediaObject->stop();
}

void MediaControll::pause(bool pause)
{
    if (mediaObject->getState() != STATE_PLAY)
    {
        return;
    }

    isPaused = pause;
    processThread->setPaused(pause);
    playButton->setEnabled(pause);
}

void MediaControll::setFrameDropping(int frameDrop)
{
    processThread->setFrameDropping(frameDrop);
}

void MediaControll::setTitle(const QString &title)
{
    videoPort->setTitle(title);
}

void MediaControll::setAutoReplay(bool doAutoReplay)
{
    this->autoRepeat = doAutoReplay;
}


//// Public Slots //////////////////////////////////////////////////////////////////////////////////


//// Private Slots /////////////////////////////////////////////////////////////////////////////////
void MediaControll::on_reloadButton_clicked()
{
    doReload = true;
    stop();
}

void MediaControll::on_playButton_clicked()
{
    play();
}

void MediaControll::on_stopButton_clicked()
{
    forceStop = true;
    stop();
}

void MediaControll::onFinished()
{
    stopButton->setEnabled(false);
    playButton->setEnabled(true);
    reloadButton->setEnabled(false);

    if (doReload || (autoRepeat && !forceStop))
    {
        doReload = false;
        play();
    }
    else
    {
        emit finished();
    }

    forceStop = false;
}


void MediaControll::onError(int error)
{
    stopButton->setEnabled(false);
    playButton->setEnabled(true);
    reloadButton->setEnabled(false);

    if (error != QNetworkReply::OperationCanceledError)
    {
        doReload = true;
    }
    else
    {
        qDebug() << "Network error: QNetworkReply::OperationCanceledError";
    }

    emit httpError(error, mediaObject->getUrl(), videoPort->getTitle());
}

void MediaControll::onVideoPortMouseEnter()
{
    controlButtonsWidget->setVisible(true);
}

void MediaControll::onVideoPortMouseLeave()
{
    controlButtonsWidget->setVisible(false);
}

void MediaControll::onVideoPortClicked(Qt::MouseButton button)
{
    emit mouseClicked(button, mediaObject->getUrl(), videoPort->getTitle());
}

