#include <QHttp>
#include <QUrl>
#include <QTemporaryFile>
#include <QDebug>

#include "camslist.h"
#include "settings.h"

CamsList::CamsList(QObject *parent) :
    QObject(parent)
{
    update();
}

void CamsList::update()
{
    QHttp          http;
    QUrl           url(Settings::instance()->getParam("core.cams.uri").toString());
    QTemporaryFile temp;

    // Get file list...
    http.setHost(url.host(), url.port(80));
    http.get(url.path(), &temp);

    while (http.currentId() != 0)
    {
        qApp->processEvents();
    }

    temp.close();
    temp.setTextModeEnabled(true);

    if (temp.open())
    {
        camsList.clear();
        QTextStream in(&temp);
        QString line;
        while (!(line = in.readLine()).isNull())
        {
            QStringList tokens = line.split("|||");
            if (tokens.size() >= 2)
            {
                CamInfo camInfo = {
                    tokens[1],
                    tokens[0]
                };
                camsList.append(camInfo);

                // Dump
                //qDebug() << tokens;
            }
        }
    }
    else
    {
        // error
    }


}

const QList<CamInfo> &CamsList::getCams()
{
    return camsList;
}

const CamInfo &CamsList::getCamByUrl(const QString &url)
{
    for (int i = 0; i < camsList.count(); ++i)
    {
        if (camsList.at(i).url == url)
        {
            return camsList.at(i);
        }
    }

    return empty;
}
