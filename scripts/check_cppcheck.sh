#!/bin/bash

list=`find .. -name '*.cpp'`
includes=`cat check_cppcheck.includes  | xargs -I'{}' echo -I \"'{}'\"`

for add_arg in "" "--check-config"
do
    cppcheck -v --template "gcc" --enable=all $includes $add_arg $@ $list
    echo
    echo
done

